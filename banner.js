// loading
/**
 *
 * @param {HTMLElement} parentElement - root element of a section or document.body element
 */
function waitAssets(parentElement) {
    if (window.pageYOffset) {
        window.scroll(0, 0);
    }
    document.body.classList.add("disable-scroll");

    function removeLoading() {
        let loading = document.querySelector("#loading");
        return new Promise(function (resolve, reject) {
            if (loading) {
                let keyFrame = new KeyframeEffect(
                    loading,
                    [{ opacity: 1 }, { opacity: 0 }],
                    {
                        duration: 500,
                        easing: "ease",
                    }
                );
                let animation = new Animation(keyFrame, document.timeline);

                animation.addEventListener("finish", function () {
                    loading.style.zIndex = -1;
                    loading.style.display = "none";
                    resolve();
                });

                animation.play();
            } else {
                reject("No loading section found.");
            }
        });
    }

    /**
     *
     * @param {[HTMLImageElement] | [HTMLVideoElement]} array - array of images or videos
     * @param {"video" | "img"} type - type of element
     */
    function asyncForEach(array, type) {
        let loaded = false;
        return new Promise(function (resolve) {
            array.forEach(function (ele, index) {
                if (type == "img") {
                    if (ele.complete) {
                        loaded = true;
                    } else {
                        ele.addEventListener("load", function () {
                            loaded = true;
                        });
                    }
                } else if (type == "video") {
                    if (ele.readyState === 4) {
                        loaded = true;
                    } else {
                        ele.onloadeddata = function () {
                            loaded = true;
                        };
                    }
                }

                if (index == array.length - 1) {
                    resolve(loaded);
                }
            });
        });
    }

    let imgsLoaded = false;
    let imgs = parentElement.querySelectorAll("img");
    let videosLoaded = false;
    // let videos = parentElement.querySelectorAll("video");

    return new Promise(async function (resolve, reject) {
        if (imgs && imgs.length) {
            await asyncForEach(imgs, "img").then(function (loaded) {
                if (loaded) {
                    imgsLoaded = true;
                } else {
                    reject("Error on loading images");
                }
            });
        } else {
            imgsLoaded = true;
        }

        // if (videos && videos.length) {
        //     await asyncForEach(videos, "video").then(function (loaded) {
        //         if (loaded) {
        //             videosLoaded = true;
        //         } else {
        //             reject("Error on loading videos");
        //         }
        //     });
        // } else {
        //     videosLoaded = true;
        // }

        if (imgsLoaded) {
            removeLoading()
                .then(function () {
                    document.body.classList.remove("disable-scroll");
                    resolve();
                })
                .catch(function (err) {
                    console.log(err);
                });
        }
    });
}

/*================ Intro section =================*/
// clouds
function introAnimateClouds() {
    let cloudsWrapper = document.querySelector("#intro .clouds");

    function animate(element, type) {
        let transitionObject;

        // transition for each cloud
        switch (type) {
            case "cloud-1":
                transitionObject = [
                    { transform: "translate(-50%, -50%)" },
                    { transform: "translate(-100%, -100%)" },
                ];
                break;

            case "cloud-2":
                transitionObject = [
                    { transform: "translate(50%, -50%)" },
                    { transform: "translate(100%, -100%)" },
                ];
                break;

            case "cloud-3":
                transitionObject = [
                    { transform: "translate(-10%, 30%)" },
                    { transform: "translate(-100%, 100%)" },
                ];
                break;

            default:
                transitionObject = [];
                break;
        }

        // prepare keyframe
        let keyFrameEffect = new KeyframeEffect(element, transitionObject, {
            duration: 3000,
            easing: "ease",
        });

        // animation instance
        let animation = new Animation(keyFrameEffect, document.timeline);

        // returning promise to attach finish event
        return new Promise(function (resolve, reject) {
            if (transitionObject.length) {
                animation.addEventListener("finish", function () {
                    resolve();
                });

                if (animation.playState == "running") {
                    animation.cancel();
                }

                animation.play();
            } else {
                reject(
                    "No transition in animateCloud. Check class name and type."
                );
            }
        });
    }

    if (cloudsWrapper) {
        let clouds = cloudsWrapper.querySelectorAll("img");
        if (clouds && clouds.length)
            clouds.forEach(async function (cloud) {
                if (cloud.classList.contains("cloud-1")) {
                    await animate(cloud, "cloud-1").then(function () {
                        cloud.remove();
                    });
                } else if (cloud.classList.contains("cloud-2")) {
                    await animate(cloud, "cloud-2").then(function () {
                        cloud.remove();
                    });
                } else if (cloud.classList.contains("cloud-3")) {
                    await animate(cloud, "cloud-3").then(function () {
                        cloud.remove();
                        cloudsWrapper.remove();
                    });
                }
            });
    }
}

// background
async function introAnimateBG() {
    let bgImg = document.querySelector("#intro .intro-bg img");

    function animate(element, type) {
        let distanceXtoMove;
        let transitionObject;
        let keyFrameOpt;

        // transtion and option for animations
        switch (type) {
            case "move":
                distanceXtoMove =
                    element.getBoundingClientRect().width -
                    element.parentElement.offsetWidth;
                transitionObject = [
                    { transform: "translateX(0px)" },
                    {
                        transform:
                            "translateX(-" +
                            Math.abs(distanceXtoMove) / 2 +
                            "px)",
                    },
                ];
                keyFrameOpt = {
                    duration: 6000,
                    easing: "linear",
                };
                break;

            case "fade":
                transitionObject = [{ opacity: 1 }, { opacity: 0 }];
                keyFrameOpt = {
                    duration: 2000,
                    delay: 4000,
                    easing: "ease",
                };
                break;

            default:
                transitionObject = [];
                break;
        }

        let keyFrameEffect = new KeyframeEffect(
            element,
            transitionObject,
            keyFrameOpt
        );
        let animation = new Animation(keyFrameEffect, document.timeline);

        return new Promise(function (resolve, reject) {
            if (transitionObject.length) {
                animation.addEventListener("finish", function () {
                    resolve();
                });

                if (animation.playState == "running") {
                    animation.cancel();
                }

                animation.play();
            } else {
                reject(
                    "No transition in introAnimateBG. Check class name and type."
                );
            }
        });
    }

    if (bgImg) {
        await Promise.all([
            animate(bgImg, "move"),
            animate(bgImg, "fade").then(function () {
                bgImg.parentElement.remove();
            }),
        ]);
    }
}

// content
function introAnimateContent() {
    function animate(element, height) {
        let keyFrameEffect = new KeyframeEffect(
            element,
            [
                { scale: 3, opacity: 0, maxHeight: 0 },
                { scale: 1, opacity: 1, maxHeight: height + 10 + "px" },
            ],
            {
                duration: 2000,
                delay: 4000,
                easing: "ease",
            }
        );

        let animation = new Animation(keyFrameEffect, document.timeline);

        return new Promise(function (resolve) {
            animation.addEventListener("finish", function () {
                resolve();
            });

            if (animation.playState == "running") {
                animation.cancel();
            }

            animation.play();
        });
    }

    // charactors
    let charactorsGroup = document.querySelector(
        "#intro .intro-group .charactors-group"
    );
    return new Promise(function (resolve, reject) {
        if (charactorsGroup) {
            charactorsGroup.style.opacity = 0;
            charactorsGroup.style.maxHeight = 0;

            if (window.innerWidth < 1024) {
                charactorsGroup.parentElement.classList.add("animating");
            }

            // listener for img loaded
            async function startAnimate(img) {
                let height = img.getBoundingClientRect().height;

                return new Promise(async function (resolve, reject) {
                    await animate(charactorsGroup, height)
                        .then(function () {
                            charactorsGroup.removeAttribute("style");
                            if (
                                charactorsGroup.parentElement.classList.contains(
                                    "animating"
                                )
                            ) {
                                charactorsGroup.parentElement.classList.remove(
                                    "animating"
                                );
                            }
                            introLateContent();
                            resolve();
                        })
                        .catch(function (err) {
                            reject(err);
                        });
                });
            }

            let childImg = charactorsGroup.querySelector("img");

            if (childImg && childImg.complete) {
                startAnimate(childImg).then(function () {
                    resolve();
                });
            } else {
                childImg.addEventListener("load", startAnimate(childImg));
            }
        } else {
            reject("charactors-group is not found");
        }
    });
}

// events
function introEvents() {
    let joinCommunity = document.querySelector("#intro .join-community");
    if (joinCommunity) {
        let triggerBtn = joinCommunity.querySelectorAll(".trigger");
        let arrow = joinCommunity.querySelector(".social-list .trigger");
        let socialList = joinCommunity.querySelector(".social-list ul");

        if (triggerBtn.length && socialList && arrow) {
            triggerBtn.forEach(function (btn) {
                btn.addEventListener("click", function () {
                    if (arrow.classList.contains("closed")) {
                        arrow.classList.remove("closed");
                        socialList.classList.add("show");
                    } else {
                        arrow.classList.add("closed");
                        socialList.classList.remove("show");
                    }
                });
            });
        }
    }

    let introSection = document.querySelector("#intro");
    function introSectionClick() {
        if (!introSection.classList.contains("cursor-default")) {
            introSection.classList.add("cursor-default");
        } else {
            introSection.classList.remove("cursor-default");
            let modal = introSection.querySelector(".custom-modal.show");
            if (modal) {
                modal.classList.remove("show");
                let header = document.querySelector("header");
                header.style.zIndex = 99999;
            }
        }
        let introBgImg = introSection.querySelector(".intro-bg-img");
        if (introBgImg) {
            introBgImg.style.filter = "blur(10px)";
        }

        let slider = introSection.querySelector(".custom_video_parent");
        if (slider && !slider.classList.contains("show")) {
            slider.classList.add("show");
        }

        let introGroup = introSection.querySelector(".intro-group");
        if (introGroup) {
            introGroup.style.position = "absolute";
            introGroup.style.filter = "blur(10px)";
        }

        let introText = introGroup.querySelector(".intro-text");
        if (introText) {
            introText.classList.add("hide");
        }

        let scrollDown = introSection.querySelector(".scroll-down");
        if (scrollDown) {
            scrollDown.classList.add("hide");
        }

        let closeVideoSlider = document.querySelector(".close-video-slider");
        if (closeVideoSlider) {
            closeVideoSlider.classList.add("show");

            function closeVideoSliderPos() {
                if (introSection) {
                    closeVideoSlider.style.top =
                        introSection.offsetTop + 20 + "px";
                }
            }

            closeVideoSliderPos();
            window.addEventListener("resize", closeVideoSliderPos);

            if (!closeVideoSlider.onclick) {
                closeVideoSlider.addEventListener("click", function () {
                    window.removeEventListener("resize", closeVideoSliderPos);

                    if (slider && slider.classList.contains("show")) {
                        slider.classList.remove("show");
                    }

                    if (introBgImg) {
                        introBgImg.style.filter = "blur(0px)";
                    }

                    if (introGroup) {
                        introGroup.removeAttribute("style");
                    }

                    if (introText && introText.classList.contains("hide")) {
                        introText.classList.remove("hide");
                    }

                    if (scrollDown && scrollDown.classList.contains("hide")) {
                        scrollDown.classList.remove("hide");
                    }

                    this.classList.remove("show");
                });
            }
        }

        introVideoSlider();
    }
    introSection.addEventListener("click", introSectionClick);

    let playBtns = document.querySelectorAll("#intro .play-btn");
    if (playBtns && playBtns.length) {
        playBtns.forEach(function (btn) {
            btn.addEventListener("click", function (e) {
                e.preventDefault();
                let modal = document.querySelector("#intro .custom-modal");
                if (modal && !modal.classList.contains("show")) {
                    introSection.removeEventListener(
                        "click",
                        introSectionClick
                    );
                    let iframe = modal.querySelector("iframe");
                    if(iframe) {
                        let modalContent = iframe.closest(".modal-content");

                        iframe.setAttribute("src", this.dataset.videourl);

                        function iframePos() {
                            iframe.setAttribute("width", modalContent.getBoundingClientRect().width);
                            iframe.setAttribute("height", modalContent.getBoundingClientRect().height);
                        }

                        setTimeout(function() {
                            iframePos();
                        }, 350);


                        window.addEventListener("resize", iframePos);
                    }
                    modal.classList.add("show");
                    let header = document.querySelector("header");
                    header.style.zIndex = -1;
                }
            });
        });
    }

    let modalClose = document.querySelectorAll("#intro .close-modal");
    if (modalClose && modalClose.length) {
        modalClose.forEach(function (ele) {
            ele.addEventListener("click", function () {
                let modal = this.closest(".custom-modal.show");
                if (modal) {
                    modal.classList.remove("show");
                    let header = document.querySelector("header");
                    header.style.zIndex = 99999;

                    introSection.addEventListener("click", introSectionClick);

                    let iframe = modal.querySelector("iframe");
                    if (iframe) {
                        iframe.removeAttribute("src");
                    }
                }
            });
        });
    }
}

// late content
function introLateContent() {
    let scrollDown = document.querySelector("#intro .scroll-down");
    if (scrollDown && scrollDown.classList.contains("hide")) {
        scrollDown.classList.remove("hide");
    }

    let logoWrapper = document.querySelector("#intro .logo-wrapper");
    let introText = document.querySelector("#intro .intro-group .intro-text");
    if (logoWrapper && introText && introText.classList.contains("hide")) {
        introText.classList.remove("hide");
    }

    let joinCommunity = document.querySelector("#intro .join-community");
    if (joinCommunity && joinCommunity.classList.contains("hide")) {
        joinCommunity.classList.remove("hide");
    }
}

// video slider
function introVideoSlider() {
    let slider = document.querySelector("#intro .video-slider");
    if (slider) {
        let flickSlider = new Flickity(slider, {
            pageDots: false,
            cellAlign: "center",
            wrapAround: true,
            contain: true,
        });

        setTimeout(function () {
            flickSlider.resize();
        }, 300);

        window.addEventListener("resize", function () {
            flickSlider.resize();
        });
    }
}
/*================ End of Intro section =================*/

/*================ DOM Manipulation =================*/
document.addEventListener(
    "DOMContentLoaded",
    function () {
        waitAssets(document.querySelector("body"))
            .then(function () {
                introAnimateClouds();
                introAnimateBG();
                introAnimateContent().then(function () {
                    introEvents();
                });
            })
            .catch(function (err) {
                console.log(err);
                // if (
                //     confirm(
                //         "Network error! Would you like to try reloading the page?"
                //     )
                // ) {
                //     location.reload();
                // }
            });
    },
    { once: true }
);
