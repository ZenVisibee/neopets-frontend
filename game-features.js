/*================ Game Features section =================*/
// events
function gfEvents() {
    let gfWrapper = document.querySelector("#gameFeatures");
    if (gfWrapper) {
        let gfFeatures = gfWrapper.querySelectorAll(".feature-wrapper");
        let indicator = gfWrapper.querySelector(".indicator");
        let indicatorTags = gfWrapper.querySelectorAll(".indicator a");

        function getElementTopPosition(childElement) {
            let topPosition = 0;
            let element = childElement;
            while (element) {
                topPosition += element.offsetTop;
                element = element.offsetParent;
            }
            return topPosition;
        }

        /**
         *
         * @param {HTMLElement} ele - html element
         *
         */
        function asyncScroll(ele) {
            window.scrollTo({
                left: 0,
                top: getElementTopPosition(ele),
                behavior: "smooth",
            });

            //   preventScroll for a while
            let supportsPassive = false;
            window.addEventListener(
                "test",
                null,
                Object.defineProperty({}, "passive", {
                    get: function () {
                        supportsPassive = true;
                    },
                })
            );
            function preventScroll(e) {
                if (e.cancelable) {
                    e.preventDefault();
                }
            }
            let wheelOpt = false;
            if (supportsPassive) {
                wheelOpt = {
                    passive: false,
                };
            }
            window.addEventListener("DOMMouseScroll", preventScroll, wheelOpt);
            window.addEventListener("wheel", preventScroll, wheelOpt);
            window.addEventListener("mousewheel", preventScroll, wheelOpt);
            window.addEventListener("touchmove", preventScroll, wheelOpt);

            return new Promise(function (resolve, reject) {
                setTimeout(function () {
                    window.removeEventListener(
                        "DOMMouseScroll",
                        preventScroll,
                        wheelOpt
                    );
                    window.removeEventListener(
                        "wheel",
                        preventScroll,
                        wheelOpt
                    );
                    window.removeEventListener(
                        "mousewheel",
                        preventScroll,
                        wheelOpt
                    );
                    window.removeEventListener(
                        "touchmove",
                        preventScroll,
                        wheelOpt
                    );
                    resolve();
                }, 500);
            });
        }

        if (
            gfFeatures &&
            gfFeatures.length &&
            indicator &&
            indicatorTags &&
            indicatorTags.length
        ) {
            /**
             *
             * @param {"show" | "hide"} type - "show" | "hide"
             */
            function triggerIndicator(type) {
                return new Promise(function (resolve) {
                    let transitionObject = [];
                    let keyFrameOpt = {
                        duration: 200,
                        easing: "ease",
                    };
                    switch (type) {
                        case "show":
                            transitionObject = [{ opacity: 0 }, { opacity: 1 }];
                            break;

                        case "hide":
                            transitionObject = [{ opacity: 1 }, { opacity: 0 }];
                            break;
                    }

                    if (transitionObject.length) {
                        let keyFrameEffect = new KeyframeEffect(
                            indicator,
                            transitionObject,
                            keyFrameOpt
                        );
                        let animation = new Animation(
                            keyFrameEffect,
                            document.timeline
                        );

                        animation.addEventListener("finish", function () {
                            resolve();
                        });

                        if (animation.playState == "running") {
                            animation.cancel();
                        }

                        animation.play();
                    }
                });
            }

            let preWindowTop = 0;
            function gfScrollHandler() {
                let windowBottom = window.pageYOffset + window.innerHeight;
                let windowTop = window.pageYOffset;

                setTimeout(function () {
                    preWindowTop = windowTop;
                }, 10);

                /* indicator tags */
                // gfFeatures.forEach(async function (ele) {
                //     let eleTop = getElementTopPosition(ele);
                //     let eleHeight = ele.getBoundingClientRect().height;
                //     let eleBottom = eleTop + eleHeight;

                //     if (
                //         (windowBottom >= eleTop &&
                //             windowBottom <= eleBottom &&
                //             windowTop > preWindowTop) ||
                //         (windowTop <= eleBottom &&
                //             windowTop >= eleTop &&
                //             windowTop < preWindowTop)
                //     ) {
                //         let href = "#" + ele.id;
                //         let curActive = Array.from(indicatorTags).filter(
                //             function (tag) {
                //                 return tag.classList.contains("active");
                //             }
                //         );
                //         let active = Array.from(indicatorTags).filter(function (
                //             tag
                //         ) {
                //             return tag.hash == href;
                //         });

                //         if (active.length) {
                //             if (
                //                 curActive.length &&
                //                 curActive[0] !== active[0]
                //             ) {
                //                 curActive[0].classList.remove("active");
                //             }
                //             active[0].classList.add("active");

                //             window.removeEventListener(
                //                 "scroll",
                //                 gfScrollHandler
                //             );
                //             asyncScroll(ele).then(function () {
                //                 window.addEventListener(
                //                     "scroll",
                //                     gfScrollHandler
                //                 );

                //                 if (!indicator.classList.contains("show")) {
                //                     indicator.classList.add("show");
                //                 }
                //             });

                //             // handle scroll up effect
                //             let activeFeature = Array.from(gfFeatures)
                //                 .filter(function (feature) {
                //                     return (
                //                         feature.id ==
                //                         active[0].hash.replace("#", "")
                //                     );
                //                 })[0]
                //                 .querySelector(".feature-content");

                //             if (
                //                 activeFeature &&
                //                 !activeFeature.classList.contains("animated")
                //             ) {
                //                 setTimeout(function () {
                //                     activeFeature.classList.add("animated");
                //                 }, 500);
                //             }
                //         }
                //     }
                // });
                /* indicator tags */

                /* indicator */
                // if (
                //     windowBottom > getElementTopPosition(gfWrapper) &&
                //     windowBottom >
                //         getElementTopPosition(gfWrapper) +
                //             (gfFeatures[0].clientHeight * 2) / 3 &&
                //     windowBottom <
                //         getElementTopPosition(gfWrapper) +
                //             gfWrapper.clientHeight +
                //             gfFeatures[0].clientHeight / 3
                // ) {
                //     setTimeout(async function () {
                //         if (!indicator.classList.contains("show")) {
                //             await triggerIndicator("show").then(function () {
                //                 indicator.classList.add("show");
                //             });
                //         }
                //     }, 10);
                // } else {
                //     setTimeout(async function () {
                //         await triggerIndicator("hide").then(function () {
                //             indicator.classList.remove("show");
                //         });
                //     }, 10);
                // }
                /* end of indicator */

                /* scroll to section */
                let gfTop = getElementTopPosition(gfWrapper);
                let gfHeight = gfWrapper.getBoundingClientRect().height;
                let gfBottom = gfTop + gfHeight;
                if (
                    (windowBottom >= gfTop &&
                        windowBottom <= gfBottom &&
                        windowTop > preWindowTop) ||
                    (windowTop <= gfBottom &&
                        windowTop >= gfTop &&
                        windowTop < preWindowTop)
                ) {
                    window.removeEventListener("scroll", gfScrollHandler);
                    asyncScroll(gfWrapper).then(function () {
                        if(!gfFeatures[0].querySelector(".feature-content").classList.contains("animated")) {
                            gfFeatures[0].querySelector(".feature-content").classList.add("animated");
                        }
                        window.addEventListener("scroll", gfScrollHandler);
                    });
                } 
                /* end of scroll to section */
            }
            gfScrollHandler();
            window.addEventListener("scroll", gfScrollHandler);

            /* indicator tags onclick */
            indicatorTags.forEach(function (a) {
                a.addEventListener("click", function (e) {
                    e.preventDefault();

                    let slideDireaction = "down";

                    /* link active */
                    let curActive = Array.from(indicatorTags).filter(function (
                        ele
                    ) {
                        return ele.classList.contains("active");
                    });

                    if (curActive.length && curActive[0] !== e.target) {
                        curActive[0].classList.remove("active");

                        if(Array.from(indicatorTags).indexOf(e.target) > Array.from(indicatorTags).indexOf(curActive[0])) {
                            slideDireaction = "up";
                        }
                    }

                    /* content active */
                    /**
                     *
                     * @param {HTMLElement} ele - element to apply animation
                     * @param {"up" | "down"} direction - slide up or slide down
                     * @param {"show" | "hide"} type - show or hide
                     */
                    function slideAnimation(ele, direction, type) {
                        let transObj = [];

                        if (direction == "up") {
                            if (type == "show") {
                                transObj = [
                                    { transform: "translateY(100%)" },
                                    { transform: "translateY(0%)" },
                                ];
                            } else if (type == "hide") {
                                transObj = [
                                    { transform: "translateY(0%)" },
                                    { transform: "translateY(-100%)" },
                                ];
                            }
                        } else if (direction == "down") {
                            if (type == "show") {
                                transObj = [
                                    { transform: "translateY(-100%)" },
                                    { transform: "translateY(0%)" },
                                ];
                            } else if (type == "hide") {
                                transObj = [
                                    { transform: "translateY(0%)" },
                                    { transform: "translateY(100%)" },
                                ];
                            }
                        }

                        let keyFrame = new KeyframeEffect(ele, transObj, {
                            duration: 500,
                            easing: "ease",
                        });

                        return new Promise(function (resolve, reject) {
                            if (!ele instanceof HTMLElement) {
                                reject("Slide animation error");
                            } else {
                                let animation = new Animation(
                                    keyFrame,
                                    document.timeline
                                );
                                animation.addEventListener(
                                    "finish",
                                    function () {
                                        resolve();
                                    }
                                );

                                animation.play();
                            }
                        });
                    }

                    if(!e.target.classList.contains("active")) {
                        let curSection = Array.from(gfFeatures).filter(function (
                            ele
                        ) {
                            return ele.classList.contains("active");
                        });
    
                        if (curSection.length) {
                            slideAnimation(curSection[0], slideDireaction, "hide").then(
                                function () {
                                    curSection[0].classList.remove("active");
                                    curSection[0].querySelector(".feature-content").classList.remove("animated");
                                }
                            );
                        }
    
                        let targetSection = Array.from(gfFeatures).filter(function (
                            ele
                        ) {
                            return ele.id == e.target.hash.replace("#", "");
                        });
    
                        if (targetSection.length) {
                            slideAnimation(targetSection[0], slideDireaction, "show").then(
                                function () {
                                    targetSection[0].classList.add("active");
                                    targetSection[0].querySelector(".feature-content").classList.add("animated");
                                }
                            );
                        }

                        e.target.classList.add("active");
                    }
                });
            });
        }

        /**
         *
         * @param {HTMLImgElement} eleToHide - img element to hide
         * @param {HTMLImgElement} eleToShow - img element to show
         */
        function swapImg(eleToHide, eleToShow) {
            let transitionObj = {
                duration: 300,
                easing: "ease",
            };

            let kfToHide = new KeyframeEffect(
                eleToHide,
                [{ opacity: 1 }, { opacity: 0 }],
                transitionObj
            );

            let kfToShow = new KeyframeEffect(eleToShow, [
                { opacity: 0 },
                { opacity: 1 },
            ]);

            let hideAnimation = new Animation(kfToHide, document.timeline);
            let showAnimation = new Animation(kfToShow, document.timeline);

            return new Promise(function (resolve) {
                showAnimation.addEventListener("finish", function () {
                    resolve();
                });
                hideAnimation.play();
                showAnimation.play();
            });
        }

        let modelWrapper = gfWrapper.querySelectorAll(".model");
        if (modelWrapper && modelWrapper.length) {
            modelWrapper.forEach(function (model) {
                model.addEventListener("click", function () {
                    let idleImg = this.querySelector(".idle");
                    let clickImg = this.querySelector(".click");

                    if (
                        idleImg &&
                        clickImg &&
                        !model.classList.contains("clicked")
                    ) {
                        swapImg(idleImg, clickImg).then(function () {
                            model.classList.add("clicked");

                            setTimeout(function () {
                                swapImg(clickImg, idleImg).then(function () {
                                    model.classList.remove("clicked");
                                });
                            }, 5000);
                        });
                    }
                });
            });
        }
    }
}
/*================ End of Game Features section =================*/

/*================ DOM Manipulation =================*/
document.addEventListener(
    "DOMContentLoaded",
    function () {
        gfEvents();
    },
    { once: true }
);
